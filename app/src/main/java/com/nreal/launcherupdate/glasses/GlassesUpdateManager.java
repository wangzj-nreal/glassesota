package com.nreal.launcherupdate.glasses;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import ai.nreal.glasses.control.Sdk.IEvent_cb;
import ai.nreal.glasses.control.Sdk.glasses_api;

import javax.annotation.concurrent.GuardedBy;

public class GlassesUpdateManager {

    private static final String TAG = GlassesUpdateManager.class.getSimpleName();

    private glasses_api gla_api = new glasses_api();

    @GuardedBy("mLock")
    private GlassesUpdateListener mUpdateListener;

    private final Object mLock = new Object();

    public static GlassesUpdateManager newInstance(GlassesUpdateListener updateListener) {
        return new GlassesUpdateManager(updateListener);
    }

    public GlassesUpdateManager(GlassesUpdateListener updateListener) {
        mUpdateListener = updateListener;
    }

    public void sendUpdateCommand() {
        Log.d(TAG, "glasses ota processing ...");
        // gla_api.command(0xff, "ff");
        gla_api.glasses_ota("");
    }

    public void sendCommand(int type, String cmd) {
        Log.d(TAG, "send command type = " + type + ", cmd = " + cmd);
        gla_api.command(type, cmd);
    }

    public boolean needOta(String location) {
        boolean result = gla_api.need_ota(location);
        Log.d(TAG, "need ota: " + result);
        return result;
    }

    public void connectToGlassesService(Context context) {
        gla_api.connect_glasses(context, new IEvent_cb() {

            @Override
            public void onEvent(byte[] bytes) {
                Log.d(TAG, "event " + bytes.toString());
            }

            @Override
            public void onConnect(int i) {
                Log.d(TAG, "glasses service connected");
                mUpdateListener.onGlassesConnected(i);
            }

            @Override
            public void onDisconnect() {
                Log.d(TAG, "glasses service disconnected");
                mUpdateListener.onGlassesDisconnected();
            }

            @Override
            public void onProgress(final int step) {
                Log.i(TAG, "onProgressChange step = " + step);
                mUpdateListener.onProgressChanged(step);
            }

            @Override
            public void onFinish(final boolean result) {
                if (result)
                    Log.d(TAG, "glasses ota success");
                else
                    Log.d(TAG, "glasses ota failed");

                Log.d(TAG, "glasses is completed");
                Activity activity = (Activity)context;
                activity.runOnUiThread(() -> {
                    Log.d(TAG, "now run on ui thread");
                    mUpdateListener.onCompleted(result);
                });
            }
        });
    }
}
