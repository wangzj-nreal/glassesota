package com.nreal.launcherupdate.glasses;

public interface GlassesUpdateListener {
    void onCompleted(boolean success);
    void onProgressChanged(int step);

    void onGlassesConnected(int i);
    void onGlassesDisconnected();
}
