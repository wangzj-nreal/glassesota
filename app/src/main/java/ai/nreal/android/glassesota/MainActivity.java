package ai.nreal.android.glassesota;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.nreal.launcherupdate.glasses.GlassesUpdateListener;
import com.nreal.launcherupdate.glasses.GlassesUpdateManager;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "GlassesOta";

    private boolean m_IsRunning = false;

    private GlassesUpdateListener mGlassesUpdateListener = new GlassesUpdateListener() {
        @Override
        public void onCompleted(boolean success) {

        }

        @Override
        public void onProgressChanged(int step) {

        }

        @Override
        public void onGlassesConnected(int i) {
            boolean needOta = mGlassesUpdateManager.needOta("");
            Log.d(TAG, "need ota: result = " + needOta);

            if (needOta && m_IsRunning ==    false) {
                m_IsRunning = true;

                // glasses_api.glasses_ota("");
                mGlassesUpdateManager.sendUpdateCommand();
            }
        }

        @Override
        public void onGlassesDisconnected() {

        }
    };

    private GlassesUpdateManager mGlassesUpdateManager = new GlassesUpdateManager(mGlassesUpdateListener);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Context self = this;
        Button connectBtn = findViewById(R.id.connect_btn);
        connectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "now connect to glasses service");
                mGlassesUpdateManager.connectToGlassesService(self);
            }
        });

        Button otaBtn = findViewById(R.id.ota_btn);
        otaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "now start ota");
                mGlassesUpdateManager.sendUpdateCommand();
            }
        });
    }
}
